#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int tamano = 36870912;
	srand(time(NULL));
	int* arreglo = (int*)malloc(tamano*sizeof(int));
	int random;
	clock_t inicio, final; 
	inicio = clock();
	for (int i = 0; i < tamano; ++i)
	{	
		random = rand();
		arreglo[i]=random;
		//printf("%d\n",arreglo[i] );
	}
	final = clock();
	printf("Programa 1 terminado en %.3f segundos\n",(final-inicio)/(double)CLOCKS_PER_SEC);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int tamano = 36870912;
	srand(time(NULL));
	int* arreglo = (int*)malloc(tamano*sizeof(int));
	int random;
	clock_t inicio, final; 
	inicio = clock();
	for (int i = 0; i < tamano; ++i)
	{	
		random = rand()%tamano;
		arreglo[random]=random;
		//printf("arreglo[%d]=%d\n",random,arreglo[random]);
	}
	final = clock();
	printf("Programa 2 terminado en %.3f segundos\n",(final-inicio)/(double)CLOCKS_PER_SEC);
	return 0;
}

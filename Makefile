DIR_OUTPUT = ./Programas
DIR_SRC = ./src
NOMBRE_PROGRAMA1 = Programa1
NOMBRE_PROGRAMA2 = Programa2
FLAG = -Wall
GCC = gcc

all: dir p1 p2

dir: 
# Crear carpeta"Programas"
	mkdir -p $(DIR_OUTPUT)

p1:
	$(GCC) $(DIR_SRC)/$(NOMBRE_PROGRAMA1).c -o $(DIR_OUTPUT)/$(NOMBRE_PROGRAMA1) $(FLAG)

p2:
	$(GCC) $(DIR_SRC)/$(NOMBRE_PROGRAMA2).c -o $(DIR_OUTPUT)/$(NOMBRE_PROGRAMA2) $(FLAG)
